package ru.nstu.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Commentary {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "author")
    private String author;

    @Column(name = "content")
    private String content;

    public Commentary() {

    }

}
