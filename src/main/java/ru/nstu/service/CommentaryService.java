package ru.nstu.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nstu.entity.Commentary;
import ru.nstu.repository.CommentaryRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CommentaryService {

    private final CommentaryRepository commentaryRepository;

    public void createCommentary(Commentary commentary) {
        commentaryRepository.save(commentary);
    }

    public Iterable<Commentary> getAllCommentaries() {
        return commentaryRepository.findAll();
    }

    public Optional<Commentary> getCommentaryById(Integer id) {

        return commentaryRepository.findById(id);

    }

    public void deleteCommentary(Commentary commentary) {

        commentaryRepository.delete(commentary);

    }
}
