package ru.nstu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nstu.entity.Commentary;

public interface CommentaryRepository extends CrudRepository<Commentary, Integer> {
}
