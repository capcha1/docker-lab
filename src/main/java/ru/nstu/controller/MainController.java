package ru.nstu.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.nstu.entity.Commentary;
import ru.nstu.service.CommentaryService;

import java.util.Optional;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class MainController {

    private final CommentaryService commentaryService;

    @GetMapping
    public String redirect() {
        return "redirect:/index";
    }

    @GetMapping("/addCommentary")
    public String showSignUpForm() {
        return "add-commentary";
    }

    /*@PostMapping("/addCommentary")
    public String createCommentary(
            Commentary commentary, BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "add-commentary";
        }

        commentaryService.createCommentary(commentary);

        model.addAttribute("commentary", commentaryService.getAllCommentaries());

        return "redirect:/index";
    }*/

    @PostMapping(value = "/addCommentary")
    public String createCommentary(
            @RequestParam(value = "content", defaultValue = "") String content,
            @RequestParam(value = "author", defaultValue = "") String author) throws Exception {
        ModelAndView modelAndView = new ModelAndView("index");

        Commentary commentary = new Commentary();

        commentary.setAuthor(author);
        commentary.setContent(content);

        commentaryService.createCommentary(commentary);

        return redirect();
    }

    @GetMapping("/index")
    public String showIndex(Model model) {
        model.addAttribute("Commentaries", commentaryService.getAllCommentaries());
        return "index";
    }

    @GetMapping("/update/{id}")
    public String showUpdate(@PathVariable("id") String id, Commentary commentary,
                             BindingResult result, Model model) {
        return "update-commentary";
    }

    @PostMapping("/update/{id}")
    public String updateCommentary(@PathVariable("id") String id, Commentary commentary,
                                   BindingResult result, Model model) {

        if (result.hasErrors()) {
            commentary.setId(Integer.parseInt(id));
            return "update-commentary";
        }

        Optional<Commentary> taskOptional = commentaryService.getCommentaryById(Integer.parseInt(id));

        taskOptional.ifPresent(value -> {
            commentary.setContent(value.getContent());
            commentaryService.createCommentary(commentary);
        });

        model.addAttribute("commentaries", commentaryService.getAllCommentaries());
        return "redirect:/index";
    }

    @GetMapping("/delete/{id}")
    public String deleteCommentary(@PathVariable("id") String id, Model model) {
        Commentary commentary = commentaryService.getCommentaryById(Integer.parseInt(id))
                .orElseThrow(() -> new IllegalArgumentException("Invalid Commentary Id:" + id));

        commentaryService.deleteCommentary(commentary);

        model.addAttribute("Commentaries", commentaryService.getAllCommentaries());

        return "redirect:/index";
    }

}
